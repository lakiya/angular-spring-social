import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';

import { faGoogle, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';

import { GOOGLE_AUTH_URL, FACEBOOK_AUTH_URL } from '../../constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = new User();
  errorMessage: string;

  faGoogle = faGoogle;
  faFacebookF = faFacebookF;
  faUser = faUser;
  faLock = faLock;
  GOOGLE_AUTH_URL = GOOGLE_AUTH_URL;
  FACEBOOK_AUTH_URL = FACEBOOK_AUTH_URL;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    if (this.userService.currentUserValue) {
      this.router.navigate(['/home']);
      return;
    }
  }

  login() {
    this.userService.login(this.user).subscribe(data => {
      this.userService.getUser().subscribe(
        () => { this.router.navigate(['/home']); }
      );
    }, err => {
      console.log(err);
      this.errorMessage = err;
    });
  }

}

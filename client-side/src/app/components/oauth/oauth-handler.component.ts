import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ACCESS_TOKEN } from 'src/app/constants';
import { UserService } from 'src/app/services/user.service';

@Component({
    template: ''
})
export class OAuthHandlerComponent {
    constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) {
        this.route.queryParams
            .subscribe(params => {
                var token = params['token'];
                localStorage.setItem(ACCESS_TOKEN, token);
                this.userService.getUser().subscribe(
                    () => { this.router.navigate(['/home']); }
                );
            });
    }
}
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { Router } from '@angular/router';

import { faGoogle, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faUser, faLock, faFile } from '@fortawesome/free-solid-svg-icons';

import { GOOGLE_AUTH_URL, FACEBOOK_AUTH_URL } from '../../constants';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = new User();
  errorMessage: string;

  faGoogle = faGoogle;
  faFacebookF = faFacebookF;
  faUser = faUser;
  faLock = faLock;
  faFile = faFile;
  GOOGLE_AUTH_URL = GOOGLE_AUTH_URL;
  FACEBOOK_AUTH_URL = FACEBOOK_AUTH_URL;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    if (this.userService.currentUserValue) {
      this.router.navigate(['/home']);
      return;
    }
  }

  register() {
    this.userService.register(this.user).subscribe(data => {
      this.router.navigate(['/login']);
    }, err => {
      if (!err || err.status !== 409) {
        this.errorMessage = "Unexpected error occurred. Error : " + err;
      } else {
        this.errorMessage = "Username/Email is already exist";
      }
    });
  }

}

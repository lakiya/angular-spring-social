import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { ACCESS_TOKEN, CURRENT_USER } from '../constants';

let API_URL = "http://localhost:8765/api/user/service/";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public currentUser: Observable<User>;
  private currentUserSubject: BehaviorSubject<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(user: User): Observable<any> {
    var loginRequest = {
      'email': user.username,
      'password': user.password
    }

    return this.http.post<any>(API_URL + "login", loginRequest).pipe(
      map(response => {
        if (response) {
          localStorage.setItem(ACCESS_TOKEN, response.accessToken);
        }
        return response;
      })
    );
  }

  logOut() {
    localStorage.removeItem(ACCESS_TOKEN);
    localStorage.removeItem(CURRENT_USER);
    this.currentUserSubject.next(null);
  }

  register(user: User): Observable<any> {
    user.email = user.username;
    return this.http.post(API_URL + "registration", JSON.stringify(user),
      { headers: { "Content-Type": "application/json; charset=UTF-8" } });
  }

  getUser() {
    return this.http.get<any>(API_URL + "me").pipe(
      map(response => {
        if (response) {
          localStorage.setItem(CURRENT_USER, JSON.stringify(response));
          this.currentUserSubject.next(response);
        }
        return response;
      })
    );
  }
}

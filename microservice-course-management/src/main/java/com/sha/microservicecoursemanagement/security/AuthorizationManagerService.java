package com.sha.microservicecoursemanagement.security;

import org.springframework.stereotype.Component;

@Component
public interface AuthorizationManagerService {
    Long getIdOfLoggedInUser();
}

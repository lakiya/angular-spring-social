package com.sha.microservicecoursemanagement.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class AuthorizationManagerServiceImpl implements AuthorizationManagerService {

    @Override
    public Long getIdOfLoggedInUser() {
        Long userId = null;
        Map<String, Object> info = getExtraInfo(getCurrentUser());
        Object shouldBeId = info.get("sub");
        try {
            userId = Long.parseLong(shouldBeId.toString());
            return userId;
        }
        catch (Exception e) {
            throw new OAuth2Exception("Could not extract user's Id from token.");
        }
    }

    private Map<String, Object> getExtraInfo(Authentication auth) {
        Object detail = auth.getDetails();
        Map<String, Object> extraInfo;
        if( detail instanceof OAuth2AuthenticationDetails) {
            OAuth2AuthenticationDetails oauthDetails = (OAuth2AuthenticationDetails) detail;
            extraInfo = (Map<String, Object>) oauthDetails.getDecodedDetails();
            return extraInfo;
        }
        if( detail instanceof Map<?, ?>) {
            extraInfo = (Map<String, Object>) detail;
            return extraInfo;
        }

        throw new OAuth2Exception("Could not extract extra info from otherwise valid JWT. Check your Token Issuer for errors.");
    }

    private Authentication  getCurrentUser() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}

package com.sha.microserviceusermanagement.controller;

import com.sha.microserviceusermanagement.exceptions.ResourceNotFoundException;
import com.sha.microserviceusermanagement.model.User;
import com.sha.microserviceusermanagement.payload.AuthResponse;
import com.sha.microserviceusermanagement.payload.LoginRequest;
import com.sha.microserviceusermanagement.payload.SignUpRequest;
import com.sha.microserviceusermanagement.security.CurrentUser;
import com.sha.microserviceusermanagement.security.TokenProvider;
import com.sha.microserviceusermanagement.security.UserPrincipal;
import com.sha.microserviceusermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;
    private final DiscoveryClient discoveryClient;
    private final Environment env;

    private final AuthenticationManager authenticationManager;
    private final TokenProvider tokenProvider;

    @Value("${spring.application.name}")
    private String serviceId;

    @Autowired
    public UserController(UserService userService,
                          DiscoveryClient discoveryClient,
                          Environment env,
                          AuthenticationManager authenticationManager,
                          TokenProvider tokenProvider) {
        this.userService = userService;
        this.discoveryClient = discoveryClient;
        this.env = env;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
    }

    @GetMapping("/service/port")
    public String getPort(){
        return "Service port number : " + env.getProperty("local.server.port");
    }

    @GetMapping("/service/instances")
    public ResponseEntity<?> getInstances(){
        return new ResponseEntity<>(discoveryClient.getInstances(serviceId), HttpStatus.OK);
    }

    @GetMapping("/service/services")
    public ResponseEntity<?> getServices(){
        return new ResponseEntity<>(discoveryClient.getServices(), HttpStatus.OK);
    }

    @PostMapping("/service/names")
    public ResponseEntity<?> getNamesOfUsers(@RequestBody List<Long> idList){
        return ResponseEntity.ok(userService.findUsers(idList));
    }

    @GetMapping("/service/me")
    @RequestMapping()
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return this.userService.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
    }

    @PostMapping("/service/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @PostMapping("/service/registration")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if(userService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        this.userService.createLocalUser(signUpRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}

package com.sha.microserviceusermanagement.model;

public enum AuthProvider {
    local,
    facebook,
    google
}

package com.sha.microserviceusermanagement.security.oauth2;

public class AuthorizationRequestCookie {
    public static final String OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME = "oauth2_auth_request";
    public static final String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";
}

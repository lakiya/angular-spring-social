package com.sha.microserviceusermanagement.service;

import com.sha.microserviceusermanagement.model.User;
import com.sha.microserviceusermanagement.payload.SignUpRequest;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User save(User user);

    void createLocalUser(SignUpRequest signUpRequest);

    List<String> findUsers(List<Long> idList);

    Optional<User> findById(Long userId);

    boolean existsByEmail(String email);
}
